import { TestBed } from '@angular/core/testing';

import { RemitancesService } from './remitances.service';

describe('RemitancesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemitancesService = TestBed.get(RemitancesService);
    expect(service).toBeTruthy();
  });
});
