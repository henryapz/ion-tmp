import { DatabaseService } from './database.service';
import { Injectable } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';

@Injectable()
export class TrackingService {
  queue: Array<Geoposition>;
  batteryLevel: any;
  subsBattery: any;
  constructor(
    private http: HttpClient,
    private geolocation: Geolocation,
    private batteryStatus: BatteryStatus,
  ) {
    this.queue = [];
    this.batteryLevel = 0;
  }

  public trackingGPS() {
    console.log("Geeeee");
    var options = {
      enableHighAccuracy: true
    };
    let a = new Date();
    console.log("Current time ", a)
    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.sendGPS(resp.coords.latitude, resp.coords.longitude);
      this.queue.push(resp);
      1
      if (this.queue.length > 5) {
        var gg = this.queue.shift();
      }
      console.log("Pila de coordenadas", this.queue)
      this.stay();
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  stay() {
    console.log("Pila: ", this.queue)
    if (this.queue.length > 1) {
      var prom = 0;
      for (let index = 0; index < (this.queue.length - 1); index++) {
        var e = this.queue[index];
        var e2 = this.queue[index + 1];
        var dist = this.distanceInKmBetweenEarthCoordinates(e.coords.latitude, e.coords.longitude, e2.coords.latitude, e2.coords.longitude) * 1000;
        prom = prom + dist;
      }
      prom = prom / this.queue.length;
      console.log(prom);
      if (prom > 20) {
        this.sendEventAlert("S","Conductor Detenido por mucho tiempo");
      }
    }
  }

  async sendEventAlert(type:any, observation:any) {
    console.log("send position alert to server");

    let urlEvent = "http://52.116.35.77:443/api/programming/mb/alert/";
    await this.geolocation.getCurrentPosition().then((resp) => {
        const data: JSON = <JSON><unknown>{
          "code": localStorage.getItem("codeweb"),
          "type": type,
          "latitude": "" + resp.coords.latitude,
          "longitude": "" + resp.coords.longitude,
          "observation": observation
        }

        var toPostWithData = {
          "data": JSON.stringify(data)
        };
        console.log("Stay alert to post", toPostWithData);
        
        this.http.post(urlEvent, toPostWithData,
          {
            headers: new HttpHeaders({
              'content-type': "application/json",
            })
          }).subscribe(data => {
            console.log(data);
          });
          
      

    }).catch((error) => {
      console.log('Error getting location', error);
      alert("GPS is not activated or the signal is poor")
    });
  }

  continuar = true;
  startBackgroundGeolocation() {
    console.log("starting background service")
    const tracking = setInterval(() => {
      this.subsBattery = this.batteryStatus.onChange().subscribe(status => {
        //console.log(status.level, status.isPlugged);
        this.batteryLevel = status.level;
        console.log(this.batteryLevel + "Geeeeee");
        this.subsBattery.unsubscribe();
      });
  
      this.trackingGPS();
      console.log("Otro periodo de trackeo");
      if (!this.continuar) {
        clearInterval(tracking);
      }
    }, Number(localStorage.getItem("codeConf")) * 60 * 1000);
    //}, 3 * 60 * 1000);
  }

  stopBackGroundService(){
    this.continuar = false;
  }

  sendGPS(latitude, longitude) {
    console.log("send position alert to server");
    let urlEvent = "http://52.116.35.77:443/api/programming/mb/status/";
    // watch change in battery status
    //if(this.batteryLevel != 0 && this.batteryLevel <= 80){
    if(this.batteryLevel != 0 && this.batteryLevel <= localStorage.getItem("battery")){
      console.log("Enviar alerta de bateria");
      this.sendEventAlert("B","Bateria bajo el nivel indicado");
    }

    if (this.batteryLevel != 0 && this.batteryLevel >= localStorage.getItem("battery")) {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        const data: JSON = <JSON><unknown>{
          "CODEOT": localStorage.getItem("codeweb"),
          "TYPE": "B",
          "LATITUDE": "" + latitude,
          "LONGITUDE": "" + longitude,
          "BATTERY": this.batteryLevel,
          "STATUS": "1",
          "DATETIME": date
        }
        var toPostWithData = {
          "data": JSON.stringify(data)
        };
        console.log("Tracking alert to post", toPostWithData);
        
        this.http.post(urlEvent, toPostWithData,
          {
            headers: new HttpHeaders({
              'content-type': "application/json",
            })
          }).subscribe(data => {
            console.log(data);
          });
        
      
    }
  }

  degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
  }

  distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371;

    var dLat = this.degreesToRadians(lat2 - lat1);
    var dLon = this.degreesToRadians(lon2 - lon1);

    lat1 = this.degreesToRadians(lat1);
    lat2 = this.degreesToRadians(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return earthRadiusKm * c;
  }
}

