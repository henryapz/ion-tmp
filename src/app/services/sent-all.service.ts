import { File } from '@ionic-native/file/ngx';
import { DatabaseService } from './database.service';
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Events } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class SentAllService implements OnInit{

  codewebGeneral: any;
  shiptoGeneral: any;
  localEvents = [];
  photos = [];
  urlEvent = "http://52.116.35.77:443/api/programming/mb/event/";
  //urlEvent =  "http://192.168.0.100:8000/processing/"
  constructor(
    private file: File,
    public http: HttpClient,
    public database: DatabaseService,
    private eventosangular: Events,
  ) {

  }
  
  ngOnInit() {
    
  }

  async send() {
    this.codewebGeneral = localStorage.getItem("codeweb");
    this.shiptoGeneral = localStorage.getItem("shipto");
    this.localEvents = []
    this.photos = [];

    await this.getLocalEvents();
    await this.getPhotos();
    console.log("Local events for sent", this.localEvents)
    console.log("Photos events for sent", this.photos)

    this.eventosangular.unsubscribe('readyToSend');
    this.eventosangular.subscribe('readyToSend', updatedData => {
      console.log("Entre aqui")
      var allevents;
      console.log("Se enviarán eventos: ", this.localEvents.length )
      for (let j = 0; j < this.localEvents.length; j++) {
        const eleEvent = this.localEvents[j];
        console.log("WTF",this.photos)
        var photos = [];
        for (let index = 0; index < this.photos.length; index++) {
          const elePhoto = this.photos[index];
          
          if (eleEvent.codeEvent == elePhoto.eventcode) {
            photos.push({"image":elePhoto.base64,"observation":elePhoto.observation})
          }
        }
        var data = {
          "OT": eleEvent.ot,
          "shipto": eleEvent.shipto,
          "codeEvent": eleEvent.codeEvent,
          "codeType": eleEvent.codeType,
          "latitude": eleEvent.latitude,
          "longitude": eleEvent.longitude,
          "process": eleEvent.process,
          "datetime": eleEvent.datetime,
          "codeConf": localStorage.getItem("codeConf"),
          "details": photos
        }
        
        console.log("JSON to send", data)
        if(allevents == undefined){
          allevents = JSON.stringify(data)
        }else{
          allevents = allevents + "," +JSON.stringify(data)
        }
      }

      if(this.localEvents.length > 0){
        console.log("Events", allevents)
        var toPostWithData = {
          "data": "[" + allevents + "]" 
        };
        console.log("Data to sent", toPostWithData)    
        this.http.post(this.urlEvent, toPostWithData,
          {
            headers: new HttpHeaders({
              'content-type': "application/json",
            })
          }).subscribe(data => {
            console.log(data);
          },error  => {
            console.log("Rrror", error, toPostWithData);
          }
          );
        this.updateEventsAlreadySent();
        alert("Eventos sincronizados")
      }else{
        alert("Todos los eventos están sincronizados")
      }

      this.eventosangular.unsubscribe('readyToSend');
    });

    this.eventosangular.publish('readyToSend', 'incompleto');

  }
  async updateEventsAlreadySent(){
    var event = {
      "ot":this.codewebGeneral,
      "shipto": this.shiptoGeneral,
      "status": "3"
    }
    await this.database.updateLocalEventStatus(event).then(res =>{
      console.log(res)
    })
  }
  async getLocalEvents() {
    await this.database.getLocalEventByOT(this.codewebGeneral).then((res: any) => {
      for (let index = 0; index < res.length; index++) {
        const element = res[index];
        if (element.status === "2" && element.shipto == localStorage.getItem("shipto")) {
          var eachElem =
          {
            "ot": element.ot,
            "shipto": element.shipto,
            "codeEvent": element.codeEvent,
            "codeType": element.codeType,
            "process": element.process,
            "latitude": element.latitude,
            "longitude": element.longitude,
            "status": element.status,
            "name": element.name,
            "datetime": element.datetime
          }
          this.localEvents.push(eachElem);
        }
      }
    });
    console.log("Local events to send", this.localEvents);
  }
  async getPhotos() {
    await this.database.getPhotos().then( async (res: any) => {
      for (let index = 0; index < res.length; index++) {
        const element = res[index];
        if (element.shipto == localStorage.getItem("shipto")){
          var data = element.path;
          var filename = data.substring(data.lastIndexOf('/') + 1);
          var path = data.substring(0, data.lastIndexOf('/') + 1);
          await this.file.readAsDataURL(path, filename).then(rpt => {
            var eachElem = {
              "code": element.code,
              "shipto": element.shipto,
              "eventcode": element.eventcode,
              "path": element.path,
              "observation": element.observation,
              "status": element.status,
              "base64": rpt
            }
            this.photos.push(eachElem);
          });
        }
      }
    });
    
    console.log("Photos to send", this.photos)
  }
}
