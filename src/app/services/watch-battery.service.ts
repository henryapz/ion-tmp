import { DatabaseService } from './database.service';
import { Injectable } from '@angular/core';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class WatchBatteryService {

  constructor(
    private batteryStatus: BatteryStatus,
    private geolocation: Geolocation,
    private http: HttpClient,
    private dbService: DatabaseService
  ) {
    this.batteryLevel = 0;
  }
  subsBattery: any;
  batteryLevel: any;
  ots = [];

  getOTsFromDataBase() {
    this.dbService.getAllOTs().then((res: any) => {
      console.log("OTS from service for watch service: " + res)
      this.ots = []
      res.forEach(element => {
        console.log("ot ", element)
        this.ots.push(element)
      })
    }).catch(console.log)
  }
  async getCurrentBattery() {

    // watch change in battery status
    this.subsBattery = this.batteryStatus.onChange().subscribe(status => {
      //console.log(status.level, status.isPlugged);
      this.batteryLevel = status.level;
      console.log(this.batteryLevel + "Geeeeee");
      this.subsBattery.unsubscribe();
    });
    console.log(this.batteryLevel + "Gaaaaaa");
    var options = {
      enableHighAccuracy: true
    };
    if (this.batteryLevel != 0) {
      console.log("send battery alert to server");
      let urlEvent = "http://52.116.35.77:443/api/programming/mb/status/";
      await this.geolocation.getCurrentPosition(options).then((resp) => {
        for (let index = 0; index < this.ots.length; index++) {
          var today = new Date();
          var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
          const data: JSON = <JSON><unknown>{
            "CODEOT": this.ots[index].CODE,
            "TYPE": "B",
            "LATITUDE": "" + resp.coords.latitude,
            "LONGITUDE": "" + resp.coords.longitude,
            "BATTERY": this.batteryLevel,
            "STATUS": "1",
            "DATETIME": date
          }

          var toPostWithData = {
            "data": JSON.stringify(data)
          };
          console.log("Battery alert to post", toPostWithData);
          this.http.post(urlEvent, toPostWithData,
            {
              headers: new HttpHeaders({
                'content-type': "application/json",
              })
            }).subscribe(data => {
              console.log(data);
            });

        }

      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }
  }
  stopBatteryService() {
    this.subsBattery.unsubscribe();
  }
  startBatteryWatcher() {
    console.log("Stating battery service")
    this.getOTsFromDataBase();
    const tracking = setInterval(() => {
      this.getCurrentBattery();
    }, 30000);
  }
}
