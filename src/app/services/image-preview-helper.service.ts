import { Injectable } from '@angular/core';

@Injectable()
export class ImagePreviewHelperService {
  private imgPath;
  constructor() { }

  setImgPath(data: any) {
    this.imgPath = data;
  }
  getImgPath() {
    return this.imgPath;
  }
}
