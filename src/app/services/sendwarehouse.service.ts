import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SendwarehouseService {
  datocodificado: any;
  datoscaneado: {};
  constructor(
    private barcodeScanner: BarcodeScanner,
    public http: HttpClient
  ) { }

  LeerCode() {
    var allData;
    try {
      var code = localStorage.getItem("codeweb")
        this.barcodeScanner.scan().then(barcodeData => {
          this.datoscaneado = barcodeData;
          console.log("dato escaneado", this.datoscaneado)
          if (!barcodeData.cancelled) {
            allData = barcodeData.text.split("/");
            console.log("Data splitted readed from qr scanner", allData);
            var url = "http://52.116.35.77:443/api/sentry/statusOTransport/";
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;
            var postData = {
              "CODEOT": code,
              "SITE": allData[0],
              "CODESITE": allData[1],
              "DATETIME": dateTime,
              "TYPE": ""
            };

            this.http.post(url, postData,
              {
                headers: new HttpHeaders({ "Content-Type": "application/json" })
              }).subscribe(data => {
                console.log(data);
              });
          }
        })
          .catch(err => {
            console.log("Error", err);
          });
      
    } catch (error) {

    }
  }
}
