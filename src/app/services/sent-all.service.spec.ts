import { TestBed } from '@angular/core/testing';

import { SentAllService } from './sent-all.service';

describe('SentAllService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SentAllService = TestBed.get(SentAllService);
    expect(service).toBeTruthy();
  });
});
