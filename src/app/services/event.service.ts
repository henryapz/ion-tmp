import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(

    private auth: AuthService,
    private http: HttpClient,
    private geolocation: Geolocation

  ) { }

  getEvents(data: any) {

  }

  async sendEventoToServer(CODEWEB: any, ELEMENT: any, OBSERVATION: any, IMAGE: any, itemCode: any) {
    let urlEvent = "http://52.116.35.77:443/api/programming/mb/tracking/event/";

    var options = {
      enableHighAccuracy: true
    };
    await this.geolocation.getCurrentPosition(options).then((resp) => {
      const data: JSON = <JSON><unknown>{
        "code": CODEWEB,
        "type": ELEMENT,
        "latitude": "" + resp.coords.latitude,
        "longitude": "" + resp.coords.longitude,
        "observation": OBSERVATION,
        "image": IMAGE,
        "item": itemCode
      }

      var toPostWithData = {
        "data": JSON.stringify(data)
      };
      console.log(toPostWithData);
      this.http.post(urlEvent, toPostWithData,
        {
          headers: new HttpHeaders({
            'content-type': "application/json",
          })
        }).subscribe(data => {
          console.log(data);
        });
    }).catch((error) => {
      console.log('Error getting location', error);
      alert("GPS is not activated or the signal is poor")
    });


  }
  getEventsFromServer() {
    const url = "http://52.116.35.77:443/api/configuration/mb/event/";
    return this.http.get(url,
      {
        headers: new HttpHeaders({ "Content-Type": "application/json" })
      }).pipe(map(data => data));
  }

  async getEventLocation() {
    /*
    var options = {
      enableHighAccuracy: true
    };
    var a;
    await this.geolocation.getCurrentPosition(options).then((resp) => {
      a = resp;
    })
    return a;
    */
  }
}
