
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Platform } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { from } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authState = new BehaviorSubject(false);// ya que solo existira un usuario, entonces esto sera true cuando se añada aun usuario y falso cuando se elimine
  getToken: Observable<any>;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private platform: Platform,
    private router: Router,
  ) {
    this.ifLoggedIn();
  }

  loginUser(data: any) {
    const url_login_api = "http://52.116.35.77:443/api/programming/mb/login/driver/";

    console.log("Data recibida del formulario", data);

    const toSend: JSON = <JSON><unknown>{
      "license": data.license,
      "plate": data.plate
    }
    console.log(data);

    return this.http.post(url_login_api, toSend,
      {
        headers: new HttpHeaders({
          'content-type': "application/json",
        })
      }).pipe(map(data => data));
  }
  setToken(data: any) {
    this.storage.set("USER", JSON.stringify(data)).then(res => {
      console.log("storageeee", res)
      this.authState.next(true);
      this.ifLoggedIn();
      this.router.navigate(["main-page"])
    })
  }

  ifLoggedIn() {
    this.getToken = from(this.storage.get('USER').then(token => {
      //maybe some processing logic like JSON.parse(token)
      return token;
    }));
  }


  logout() {
    this.storage.remove('USER').then(() => {
      localStorage.setItem("intervalID", "false");
      this.router.navigate(['login']);
      this.authState.next(false);
    });
  }

  isAuthenticated() {
    return this.authState.value;
  }
}