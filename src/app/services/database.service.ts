import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Injectable, OnInit } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  database: SQLiteObject = null;
  constructor(
    private transfer: FileTransfer,
    private file: File
  ) {

  }

  setDatabase(db: SQLiteObject) {
    if (this.database === null) {
      this.database = db
    }
  }
  createTables() {
    let sql = 'CREATE TABLE IF NOT EXISTS user(id TEXT, user TEXT, password TEXT, email TEXT, license TEXT, plate TEXT, company TEXT, active TEXT, fname TEXT, lname TEXT, dni TEXT)'
    //Drives table
    let sqlDriver = 'CREATE TABLE IF NOT EXISTS driver(PROVIDER TEXT, LICENSE TEXT, FULLNAME TEXT, DOCUMENT TEXT, VEHICLE TEXT, TRACT TEXT, TUMBRIL TEXT, QR TEXT)'
    //New tables for OTS
    let sqlOTs = 'CREATE TABLE IF NOT EXISTS ots(POSITION TEXT,CODE TEXT,DATEINI TEXT,CENTER TEXT,NCENTER TEXT,LATCENTER TEXT,LONCENTER TEXT,SOLDTO TEXT,NSOLDTO TEXT,SHIPTO TEXT,NSHIPTO TEXT,ADDRESS TEXT,LATSHIPTO TEXT,LONSHIPTO TEXT,CONTACT TEXT,CONTACTPHONE TEXT,CONTACTMAIL TEXT, DATEFIN TEXT, STATUS TEXT)'
    //New table for OTS Details
    let sqlOTsDetails = 'CREATE TABLE IF NOT EXISTS otdetail(CODE TEXT,SHIPTO TEXT, MATNR TEXT, NMATNR TEXT, BRGEW TEXT)'
    //Table for remitances of OTS obsolete
    let sqlRemitances = 'CREATE TABLE IF NOT EXISTS remitance(CODEWEB TEXT, CODE TEXT, KUNNR TEXT,NKUNNR TEXT,KUNNR_S TEXT,MATNR TEXT,NMATNR TEXT,NTGEW TEXT,BRGEW TEXT,MSG01 TEXT,VSBED TEXT,VSTEL TEXT,GGRR TEXT,INVOCE TEXT,OORDER TEXT,DISTANCE TEXT,DURATION TEXT,ADDRESS TEXT,LATITUDE TEXT, LONGITUDE TEXT)';
    //Table for events general
    let sqlEvents = 'CREATE TABLE IF NOT EXISTS events(CODE TEXT, TYPE TEXT, NAME TEXT, PROCESS TEXT, POSITION TEXT, ACTIVE TEXT)'
    //Old table for ots -- to deleted(on progress)
    let sqlOt = 'CREATE TABLE IF NOT EXISTS ot(id TEXT, proDistance TEXT, proCode TEXT, startLocation TEXT, startAddress TEXT, startLatitude TEXT, startLongitude TEXT, endLocation TEXT, endAddress TEXT, endLatitude TEXT, endLongitude TEXT, startDateHour TEXT, endDateHour TEXT, proWeight TEXT, proVolume TEXT, status TEXT)'
    //Local events to push to server, not general
    let eventosLocal = "CREATE TABLE IF NOT EXISTS localevent(ot TEXT, shipto TEXT, codeEvent TEXT, codeType TEXT, process TEXT, latitude TEXT, longitude TEXT, status TEXT, name TEXT, datetime TEXT)"
    let photos = "CREATE TABLE IF NOT EXISTS photo(code TEXT, shipto TEXT, eventcode TEXT, path TEXT, observation TEXT, status TEXT)"
    return this.database.executeSql(photos, []), this.database.executeSql(sqlOTsDetails, []), this.database.executeSql(eventosLocal, []), this.database.executeSql(sql, []), this.database.executeSql(sqlEvents, []), this.database.executeSql(sqlOt, []), this.database.executeSql(sqlDriver, []), this.database.executeSql(sqlOTs, []), this.database.executeSql(sqlRemitances, [])

  }

  //Photo
  addPhoto(photo: any) {
    console.log("Object recived in the data base", photo);
    let sqlPhoto = 'INSERT INTO photo(code, shipto, eventcode, path, observation, status) VALUES (?,?,?,?,?,?)'
    let data = [photo.code, photo.shipto, photo.eventcode, photo.path, photo.observation, "0"]
    return this.database.executeSql(sqlPhoto, data)
  }
  getPhotos() {
    let sql = 'SELECT * FROM photo'
    return this.database.executeSql(sql, []).then(res => {
      let user = []
      for (let i = 0; i < res.rows.length; i++) {
        user.push(res.rows.item(i))
      }
      return Promise.resolve(user)
    }).catch(error => {
      Promise.reject(error)
    })
  }
  //Driver
  addDriver(driver: any) {
    console.log("Recibido en la base de datos usuario: ", driver)
    const url = "http://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=" + localStorage.getItem("codeweb");
    let fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(url, this.file.cacheDirectory + 'barcode.png').then((entry) => {
      console.log('download complete: ' + entry.toURL());
    }, (error) => {
      console.log(error)
    });
    let sql = 'INSERT INTO driver(PROVIDER, LICENSE, FULLNAME, DOCUMENT, VEHICLE, TRACT, TUMBRIL, QR) VALUES (?, ?, ?, ?, ?, ?, ?,?)'
    let data = [driver.PROVIDER, driver.LICENSE, driver.FULLNAME, driver.DOCUMENT, driver.VEHICLE, driver.TRACT, driver.TUMBRIL, this.file.cacheDirectory + 'barcode.png'];
    return this.database.executeSql(sql, data)
  }
  getDrivers() {
    let sql = 'SELECT * FROM driver'
    return this.database.executeSql(sql, []).then(res => {
      let user = []
      for (let i = 0; i < res.rows.length; i++) {
        user.push(res.rows.item(i))
      }
      return Promise.resolve(user)
    }).catch(error => {
      Promise.reject(error)
    })
  }
  getDriver(license: any) {
    return this.database.executeSql('SELECT * FROM user WHERE license = ?', [license]).then(data => {
      if (data.rows.length > 0) {
        return {
          license: data.rows.item(0).license,
          tract: data.rows.item(0).tract,
          tumbril: data.rows.item(0).tumbril,
        }
      } else {
        return null;
      }
    });
  }

  deleteDriver() {
    let query = 'DROP TABLE license';
    this.database.executeSql(query, []);
    //return this.database.executeSql(query, []);
  }

  //OT v2

  addOTs(ots: any) {
    console.log("OT recibida en la bd para insertar", ots)
    let sql = 'INSERT INTO ots(POSITION,CODE,DATEINI,CENTER,NCENTER,LATCENTER,LONCENTER,SOLDTO,NSOLDTO,SHIPTO,NSHIPTO,ADDRESS,LATSHIPTO,LONSHIPTO,CONTACT,CONTACTPHONE,CONTACTMAIL, DATEFIN, STATUS) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    let data = [ots.POSITION, ots.CODE, ots.DATEINI, ots.CENTER, ots.NCENTER, ots.LATCENTER, ots.LONCENTER, ots.SOLDTO, ots.NSOLDTO, ots.SHIPTO, ots.NSHIPTO, ots.ADDRESS, ots.LATSHIPTO, ots.LONSHIPTO, ots.CONTACT, ots.CONTACTPHONE, ots.CONTACTMAIL, ots.DATEFIN, "1"];
    return this.database.executeSql(sql, data);
  }

  getAllOTs() {
    let sql = 'SELECT * FROM ots'
    return this.database.executeSql(sql, []).then(res => {
      let user = []
      for (let i = 0; i < res.rows.length; i++) {
        user.push(res.rows.item(i))
      }
      return Promise.resolve(user)
    }).catch(error => {
      Promise.reject(error)
    })
  }

  updateOTStatus(event) {
    let sql = "UPDATE ots SET STATUS = '" + event.status + "' where SHIPTO = '" + event.shipto + "'";
    return this.database.executeSql(sql, [])
  }

  addOTsDetail(otdetail: any) {
    console.log("Add ot details", otdetail)
    let sql = 'INSERT INTO otdetail (CODE, SHIPTO, MATNR, NMATNR, BRGEW) VALUES (?,?,?,?,?)'
    let data = [otdetail.CODE, otdetail.SHIPTO, otdetail.MATNR, otdetail.NMATNR, otdetail.BRGEW];
    return this.database.executeSql(sql, data);
  }

  getAllOTsDetail() {
    let sql = 'SELECT * FROM otdetail'
    return this.database.executeSql(sql, []).then(res => {
      let user = []
      for (let i = 0; i < res.rows.length; i++) {
        user.push(res.rows.item(i))
      }
      return Promise.resolve(user)
    }).catch(error => {
      Promise.reject(error)
    })
  }

  //Remitances
  addRemitances(code: any, dataRm: any) {
    let sql = 'INSERT INTO remitance(CODEWEB, CODE, KUNNR,NKUNNR,KUNNR_S,MATNR,NMATNR,NTGEW,BRGEW,MSG01,VSBED,VSTEL,GGRR,INVOCE,OORDER,DISTANCE,DURATION,ADDRESS,LATITUDE, LONGITUDE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    let data = [code, dataRm.CODE, dataRm.KUNNR, dataRm.NKUNNR, dataRm.KUNNR_S, dataRm.MATNR, dataRm.NMATNR, dataRm.NTGEW, dataRm.BRGEW, dataRm.MSG01, dataRm.VSBED, dataRm.VSTEL, dataRm.GGRR, dataRm.INVOCE, dataRm.ORDER, dataRm.DISTANCE, dataRm.DURATION, dataRm.ADDRESS, dataRm.LATITUDE, dataRm.LONGITUDE];
    return this.database.executeSql(sql, data)
  }
  getRemitances(codeweb: any) {
    return this.database.executeSql('SELECT * FROM remitance WHERE CODEWEB = ? ORDER BY OORDER DESC', [codeweb]).then(res => {
      let user = []
      for (let i = 0; i < res.rows.length; i++) {
        user.push(res.rows.item(i))
      }
      return Promise.resolve(user)
    }).catch(error => {
      Promise.reject(error)
    })
  }
  //User
  addUser(user: any) {

    let sql = 'INSERT INTO user(id, user, password, email, license, plate, company, active, fname, lname, dni) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    let data = [user.id, user.user, user.password, user.email, user.license, user.plate, user.company, user.active, user.fname, user.lname, user.dni];
    return this.database.executeSql(sql, data)
  }

  getUsers() {
    let sql = 'SELECT * FROM user'
    return this.database.executeSql(sql, []).then(res => {
      let user = []
      for (let i = 0; i < res.rows.length; i++) {
        user.push(res.rows.item(i))
      }
      return Promise.resolve(user)
    }).catch(error => {
      Promise.reject(error)
    })
  }


  getUser(id: any) {
    return this.database.executeSql('SELECT * FROM user WHERE id = ?', [id]).then(data => {
      if (data.rows.length > 0) {
        return {
          id: data.rows.item(0).id,
          user: data.rows.item(0).user,
          password: data.rows.item(0).password,
          email: data.rows.item(0).email,
          license: data.rows.item(0).license,
          plate: data.rows.item(0).plate,
          company: data.rows.item(0).company,
          active: data.rows.item(0).active,
          fname: data.rows.item(0).fname,
          lname: data.rows.item(0).lname,
          dni: data.rows.item(0).dni,
        }
      } else {
        return null;
      }
    });
  }

  // Storage Events from service
  addEvent(event: any) {
    console.log("event to be added to the database", event)
    let sql = 'INSERT INTO events(CODE, TYPE, NAME, PROCESS, POSITION, ACTIVE) VALUES (?, ?, ?, ?, ?, ?)'
    let data = [event.CODE, event.TYPE, event.NAME, event.PROCESS, event.POSITION, event.ACTIVE];
    return this.database.executeSql(sql, data)
  }

  getEvents() {
    let sql = "SELECT * FROM events"
    return this.database.executeSql(sql, []).then(res => {
      let event = [];
      for (let i = 0; i < res.rows.length; i++) {
        event.push(res.rows.item(i));
      }
      return Promise.resolve(event)
    }).catch(error => {
      Promise.reject(error)
    })
  }

  getEventByID(data: String) {
    let sql = "SELECT * FROM events WHERE code LIKE " + data
    return this.database.executeSql(sql, []).then(res => {
      console.log("Respuesta de where ", res)
      let event = [];
      for (let i = 0; i < res.rows.length; i++) {
        event.push(res.rows.item(i));
      }
      return Promise.resolve(event)
    }).catch(error => {
      Promise.reject(error)
    })
  }

  //ot
  addOT(ot: any) {
    let sql = 'INSERT INTO ot(id, proDistance, proCode, startLocation, startAddress, startLatitude, startLongitude, endLocation, endAddress, endLatitude, endLongitude, startDateHour, endDateHour, proWeight, proVolume, status) VALUES (?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?, ?, ?, ?)'
    let data = [ot.id, ot.proDistance, ot.proCode, ot.startLocation, ot.startAddress, ot.startLatitude, ot.startLongitude, ot.endLocation, ot.endAddress, ot.endLatitude, ot.endLongitude, ot.startDateHour, ot.endDateHour, ot.proWeight, ot.proVolume, ot.status];
    return this.database.executeSql(sql, data)
  }
  getOTs(cadena: any) {
    let sql = 'SELECT * FROM ot'

    if (cadena != "") {
      sql = sql + " WHERE proCode LIKE '%" + cadena + "%' OR startLocation LIKE '%" + cadena + "%'"
    }
    return this.database.executeSql(sql, []).then(res => {
      let event = []
      for (let i = 0; i < res.rows.length; i++) {
        event.push(res.rows.item(i))
      }
      return Promise.resolve(event)
    }).catch(error => {
      Promise.reject(error)
    })
  }

  deleteOT() {
    let query = 'DROP TABLE ot';
    this.database.executeSql(query, []);
    let sqlOt = 'CREATE TABLE IF NOT EXISTS ot(id TEXT, proDistance TEXT, proCode TEXT, startLocation TEXT, startAddress TEXT, startLatitude TEXT, startLongitude TEXT, endLocation TEXT, endAddress TEXT, endLatitude TEXT, endLongitude TEXT, startDateHour TEXT, endDateHour TEXT, proWeight TEXT, proVolume TEXT, status TEXT)';
    return this.database.executeSql(sqlOt, []);
  }

  //Local Events
  addLocalEvent(ot: any, shipto: any, event: any, latitude: any, longitude: any) {
    event.ot = ot
    event.shipto = shipto;
    console.log("to add localevent: ", event)
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = ""+date + ' ' + time;
    let sql = 'INSERT INTO localevent(ot, shipto, codeEvent, codeType, process, latitude, longitude, status, name, datetime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    let data = [event.ot, event.shipto, event.codeEvent, event.codeType, event.process, latitude + "", longitude + "", event.status || "1", event.name, dateTime + ""];
    return this.database.executeSql(sql, data)
  }

  getLocalEvents() {
    let sql = "SELECT * FROM localevent"
    return this.database.executeSql(sql, []).then(res => {
      let event = [];
      for (let i = 0; i < res.rows.length; i++) {
        event.push(res.rows.item(i));
      }
      return Promise.resolve(event)
    }).catch(error => {
      Promise.reject(error)
    })
  }
  getLocalEventByOT(data: String) {
    let sql = "SELECT * FROM localevent"
    return this.database.executeSql(sql, []).then(res => {
      console.log("Respuesta de where ", res)
      let event = [];
      for (let i = 0; i < res.rows.length; i++) {
        event.push(res.rows.item(i));
      }
      return Promise.resolve(event)
    }).catch(error => {
      Promise.reject(error)
    })
  }
  deleteLocalEvents() {
    let sql = "DELETE FROM  localevent"

  }
  updateLocalEvent(event) {
    console.log("event to update ", event)
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = ""+date + ' ' + time;
    let sql = "UPDATE localevent SET datetime = '" + dateTime + "', status = '" + event.status + "', latitude ='" + event.latitude + "', longitude ='" + event.longitude + "' where ot = '" + event.ot + "' AND shipto = '" + event.shipto + "' AND codeEvent = '" + event.codeEvent + "'";

    return this.database.executeSql(sql, [])
  }

  updateLocalEventStatus(event) {
    console.log(event, "to be updated")
    let sql = "UPDATE localevent SET status = '" + event.status + "' where ot = '" + event.ot + "' AND shipto = '" + event.shipto + "' AND status = '" + 2 + "'";

    return this.database.executeSql(sql, [])
  }

  destroyAllTables() {
    //Drives table
    let sqlDriver = 'DELETE FROM driver'
    //New tables for OTS
    let sqlOTs = 'DELETE FROM ots'
    let sqlOtdetail = "DELETE FROM otdetail"
    //Table for events
    let sqlEvent = 'DELETE FROM events'

    //Table for local events
    let sqlLocalEvents = 'DELETE FROM localevent'
    let sqlLocalPhoto = 'DELETE FROM photo'
    let sql = 'VACUUM'

    return this.database.executeSql(sqlOtdetail, []), this.database.executeSql(sqlLocalEvents, []), this.database.executeSql(sqlLocalPhoto, []), this.database.executeSql(sqlEvent, []), this.database.executeSql(sqlDriver, []), this.database.executeSql(sqlOTs, []), this.database.executeSql(sql, [])

  }

}
