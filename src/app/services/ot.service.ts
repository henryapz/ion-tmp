import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OtService {

  constructor(private auth: AuthService, private http: HttpClient) { }
  getOts(data: any) {
    const url_login_api = "http://52.116.35.77:443/api/programming/mb/otransport/";

    console.log("Data recibida del usuario", data);

    const toSend: JSON = <JSON><unknown>{
      "license": data.license,
      "plate": data.plate
    }
    console.log(data);

    return this.http.post(url_login_api, toSend,
      {
        headers: new HttpHeaders({
          'content-type': "application/json",
        })
      }).pipe(map(data => data));
  }
}
