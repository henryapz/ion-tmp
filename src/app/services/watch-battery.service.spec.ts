import { TestBed } from '@angular/core/testing';

import { WatchBatteryService } from './watch-battery.service';

describe('WatchBatteryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WatchBatteryService = TestBed.get(WatchBatteryService);
    expect(service).toBeTruthy();
  });
});
