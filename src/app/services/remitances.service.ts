import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RemitancesService {

  constructor(
    private http: HttpClient
  ) { }

  getRemitances(data: any) {
    console.log("CODEWEB para bajar remitances" + data)
    const url_api = "http://52.116.35.77:443/api/programming/mb/ot/detail/";
    const toSend: JSON = <JSON><unknown>{
      "code": data,
    }
    return this.http.post(url_api, toSend,
      {
        headers: new HttpHeaders({
          'content-type': "application/json",
        })
      }).pipe(map(data => data));
  }
}
