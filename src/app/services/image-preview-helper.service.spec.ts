import { TestBed } from '@angular/core/testing';

import { ImagePreviewHelperService } from './image-preview-helper.service';

describe('ImagePreviewHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImagePreviewHelperService = TestBed.get(ImagePreviewHelperService);
    expect(service).toBeTruthy();
  });
});
