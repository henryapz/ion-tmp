import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DatabaseService } from '../services/database.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  usuario: any;
  constructor(
    private dbService: DatabaseService,
    private storage: Storage,
  ) {
  }
  ngOnInit() {
    this.onLoad();
  }

  onLoad() {
    this.storage.get("USER").then(res => {
      console.log("Desde el servicio obtuve el usuario", JSON.parse(res));
      console.log("Name del usuario (para ver si jala bien)", JSON.parse(res).fname);
      this.usuario = JSON.parse(res);
    }, error => {
      console.log(error)
    });
  }

}
