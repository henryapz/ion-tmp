import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }
  canActivate(): boolean {
    let obj: any = this.authService.getToken.pipe(map(data => {
      console.log('holaaaa', data)
      if (isNullOrUndefined(data)) {
        this.router.navigate(['login'])
        return false
      } else {
        //this.router.navigate(['main-page'])
        return true
      }
    }))
    return obj
  }
}
