import { WatchBatteryService } from '../services/watch-battery.service';
import { Storage } from '@ionic/storage';
import { EventService } from './../services/event.service';
import { DatabaseService } from '../services/database.service';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { OtService } from '../services/ot.service';
import { TrackingService } from '../services/tracking.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  ots: any = []
  buscar: any = ''

  constructor(
    private nav: NavController,
    private dbService: DatabaseService,
    private menu: MenuController,
    private auth: AuthService,
    private eventService: EventService,
    private storage: Storage,
    private otService: OtService,
    private trackingService: TrackingService,
    private watchService: WatchBatteryService,
  ) {
    this.nav.navigateForward('main-page')
  }

  ngOnInit() {
    this.menu.enable(true)
  }


  openInfo() {
    this.nav.navigateForward('ot-information/')
  }
  openPoints() {
    this.nav.navigateForward('points-maps/')
  }


}
