import { WebView } from '@ionic-native/ionic-webview/ngx';
import { TrackingService } from './services/tracking.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuItemComponent } from './components/menu-item/menu-item.component';
import { SignaturePadComponent } from './components/signature-pad/signature-pad.component';
import { SignaturePadModule } from 'angular2-signaturepad'
import { HttpClientModule } from '@angular/common/http';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';
import { WatchBatteryService } from './services/watch-battery.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { MoreOtInfoComponent } from './components/more-ot-info/more-ot-info.component';
@NgModule({
  declarations: [AppComponent, MenuItemComponent, MoreOtInfoComponent, SignaturePadComponent],
  entryComponents: [MoreOtInfoComponent, SignaturePadComponent],
  imports: [BrowserModule,
    SignaturePadModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule],
  providers: [
    StatusBar,
    BarcodeScanner,
    SplashScreen,
    SQLitePorter,
    SQLite,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    BackgroundMode,
    TrackingService,
    WatchBatteryService,
    Keyboard,
    Camera,
    Base64,
    File,
    FileTransfer,
    BatteryStatus,
    Geolocation,
    WebView
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
