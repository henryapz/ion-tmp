import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { ModalController, NavController, Events } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { SignaturePadComponent } from 'src/app/components/signature-pad/signature-pad.component';

@Component({
  selector: 'app-view-preview',
  templateUrl: './view-preview.page.html',
  styleUrls: ['./view-preview.page.scss'],
})
export class ViewPreviewPage implements OnInit {

  data;
  baseImage;
  image;
  urlToShow;
  cop;
  cop2;
  inputForObservation;
  constructor(
    private file: File,
    private router: Router,
    public modalController: ModalController,
    public databaseservicio: DatabaseService,
    private geolocation: Geolocation,
    public navCtrl: NavController,
    public events: Events
  ) {

  }

  async ngOnInit() {
    this.data = localStorage.getItem("photoTakenPath")
    var filename = this.data.substring(this.data.lastIndexOf('/') + 1);
    var path = this.data.substring(0, this.data.lastIndexOf('/') + 1);
    await this.file.readAsDataURL(path, filename).then(res => {
      this.cop = res;
    });

  }
  async enviarphoto() {
    let image: String = this.cop;
    image = image.substring(image.indexOf(',') + 1, image.length)
    let d = ""
    let photo = {
      "code": localStorage.getItem("codeweb"),
      "shipto": localStorage.getItem("shipto"),
      "eventcode": localStorage.getItem("codeEvent"),
      "path": localStorage.getItem("photoTakenPath"),
      "observation": d + this.inputForObservation
    }
    console.log("data for image to be saved", photo)
    await this.databaseservicio.addPhoto(photo).then((res) => {
      console.log("updated photo for event", res);
    })

    let nextStep = localStorage.getItem("nextStep");
    localStorage.removeItem("nextStep")
    let codeweb = localStorage.getItem("codeweb");
    console.log(codeweb)
    if (nextStep != undefined && nextStep.includes("S")) {
      this.firmaRecojo();
      localStorage.removeItem("nextStep")
    } else {
      let event = {
        "ot": localStorage.getItem("codeweb"),
        "shipto": localStorage.getItem("shipto"),
        "codeEvent": localStorage.getItem("codeEvent"),
        "latitude": " ",
        "longitude": " ",
        "status": "2"
      }

      let opt = {
        enableHighAccuracy: true
      };
      await this.geolocation.getCurrentPosition(opt).then((resp) => {
        event.latitude = "" + resp.coords.latitude;
        event.longitude = "" + resp.coords.longitude;
      })

      await this.databaseservicio.updateLocalEvent(event).then((res) => {
        console.log("updated event", res);
      })
      this.events.publish('user:updated', 'guardado');
      this.events.publish('returnedFromEdit');
    }
    this.router.navigate(['ot-information']);
  }


  async firmaRecojo() {
    const modal = await this.modalController.create({
      component: SignaturePadComponent,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
}
