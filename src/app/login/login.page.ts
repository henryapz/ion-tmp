import { AuthService } from './../services/auth.service';
import { OtService } from './../services/ot.service';
import { TrackingService } from './../services/tracking.service';
import { DatabaseService } from '../services/database.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from '../services/event.service';
import { WatchBatteryService } from '../services/watch-battery.service';
import { LoadingController } from '@ionic/angular';
import { delay } from 'rxjs/internal/operators/delay';
import { NavController, MenuController, Platform } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  license: string = ''
  plate: string = ''
  ots = [];
  isLoading = false;
  constructor(
    private menuCtrl: MenuController,
    private authService: AuthService,
    private router: Router,
    public databaseuser: DatabaseService,
    private eventService: EventService,
    private trackingService: TrackingService,
    private loadingController: LoadingController,
    private otService: OtService,
    private nav: NavController
  ) {

  }

  async ngOnInit() {
    this.menuCtrl.enable(false);

    await this.isAlreadyLogged().then((res) => {

      if (res == true) {
        this.menuCtrl.enable(true);
        //this.watchBattery.startBatteryWatcher();
        this.trackingService.startBackgroundGeolocation();
        //this.router.navigate(["main-page"]);
        this.nav.navigateRoot(['main-page']);
      } else {
        this.menuCtrl.enable(false);
      }
    })


  }

  ngOnDestroy() {
    this.menuCtrl.enable(true);
  }

  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: "Recibiendo datos del servidor"
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async onLogin(form: any) {
    if (form.value.license === "" || form.value.plate === "") {
      alert("Completar todos los campos")
    } else {
      this.present();
      this.authService.loginUser(form.value).subscribe(async (rpt: any) => {
        await this.databaseuser.destroyAllTables();
        console.log("Response from authentication server: ", rpt.response);
        if (rpt.response == "success") {
          this.otService.getOts(form.value).subscribe((rptOT: any) => {
            console.log("Response from ot server", rptOT);
            if (rptOT.response == "success") {
              localStorage.setItem("codeConf",rptOT.location)
              localStorage.setItem("battery",rptOT.battery)
              console.log("Tiempo de GPS",localStorage.getItem("codeConf"))
              console.log("alerta de la bateria",localStorage.getItem("battery"))
              for (let i = 0; i < rptOT.data.length; i++) {
                localStorage.setItem("codeweb",rptOT.data[i].CODE)
                console.log(localStorage.getItem("codeweb"))
                this.onsetOTs(rptOT.data[i]);
                this.ots.push(rptOT.data[i])
                
                for (let j = 0; j < rptOT.data[i].DETAILS.length; j++) {
                  this.onsetOTDetail(rptOT.data[i].CODE, rptOT.data[i].SHIPTO, rptOT.data[i].DETAILS[j]);
                }
              }
              this.onsetEvents();
              this.onsetDriver(rpt.data);
              this.dismiss();
              this.nav.navigateRoot(['main-page']);
            }else{
              alert("Error en sincronizar OTs")
              this.dismiss()
            }
          })
        }
      }, error => {
        alert(error)
        this.dismiss();
      });
      
    }
  }



  async onsetDriver(data: any) {
    await this.databaseuser.addDriver(data).then(d => {
      console.log("inserted user ", d)
    }
    ).catch((err) => {
      console.log(err.message); // something bad happened
    });

  }

  onsetUser(data: any) {
    this.databaseuser.addUser(data)
      .then(da => {
        console.log("inserte usuario", da);
      });
  }
  onsetOTs(data: any) {
    this.databaseuser.addOTs(data)
      .then(da => {
        console.log("inserte ot", da);
      });
  }

  onsetOTDetail(code: any, shipto: any, data: any) {
    data.CODE = code;
    data.SHIPTO = shipto;
    console.log("OT detail recived to be inserted:", data)

    this.databaseuser.addOTsDetail(data)
      .then(da => {
        console.log("inserte ot detail ", da);
      }, error => {
        console.log(error)
      });
  }

  onsetEvents() {
    console.log("ots variable lenth", this.ots.length)
    this.eventService.getEventsFromServer().subscribe((eventRpt: any) => {
      if (eventRpt.response === "success") {
        console.log("Events from server ", eventRpt.data)
        console.log("Numero de tipos de eventos: ", eventRpt.data.length)
        for (let j = 0; j < eventRpt.data.length; j++) {
          if (eventRpt.data[j].CODE === "T") {
            for (let k = 0; k < eventRpt.data[j].DETAILS.length; k++) {
              this.databaseuser.addEvent(eventRpt.data[j].DETAILS[k]);
            }

          } else {
            console.log("Event to localEvent", eventRpt.data[j])
            for (let k = 0; k < this.ots.length; k++) {
              for (let l = 0; l < eventRpt.data[j].DETAILS.length; l++) {
                var hd = {
                  "codeEvent": eventRpt.data[j].DETAILS[l].CODE,
                  "codeType": eventRpt.data[j].DETAILS[l].TYPE,
                  "process": eventRpt.data[j].DETAILS[l].PROCESS,
                  "name": eventRpt.data[j].DETAILS[l].NAME
                };

                this.databaseuser.addLocalEvent(this.ots[k].CODE, this.ots[k].SHIPTO, hd, "", "")
              }
            }
          }

        }
        this.dismiss();
        this.router.navigate(["main-page"]);
      } else {
        this.dismiss();
        alert(eventRpt.response)
      }
    }, error => {
      console.log(error)
    });
  }

  async isAlreadyLogged() {
    let a = true;
    await this.databaseuser.getDrivers().then((data: any) => {
      console.log("Desde la base de datos: todos los usuarios", data)
      if (data.length === 0) {
        console.log("retorne false, que!!")
        a = false;
      } else {
        //this.watchBattery.startBatteryWatcher();
      }
    }
    )
    return a;
  }
}
