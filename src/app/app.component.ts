import { SendwarehouseService } from './services/sendwarehouse.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { DatabaseService } from './services/database.service';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Component, OnInit } from '@angular/core';
import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TrackingService } from './services/tracking.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  datocodificado: any;
  datoscaneado: {};
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private sqlite: SQLite,
    private dbservice: DatabaseService,
    private router: Router,
    public http: HttpClient,
    public backgroundMode: BackgroundMode,
    private menuCtrl: MenuController,
    private sendwarehouseservice: SendwarehouseService,
    private nav: NavController,
    private trackingService: TrackingService
  ) {
    this.initializeApp()
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.createDB();
      this.backgroundMode.enable();
      this.backgroundMode.on('activate').subscribe(() => {
        console.log('activated background mode');
        this.backgroundMode.disableWebViewOptimizations();
      });
    });
  }
  createDB() {
    this.sqlite.create({
      name: 'data1.db',
      location: 'default'
    }).then((db) => {
      console.log("entre bien")
      this.dbservice.setDatabase(db);
      return this.dbservice.createTables().then(res => {
        console.log("cree las tablas", res)
      }, error => {
        console.log("cree mal las tablas", error)
      })
    }).catch(e => console.log("error al crear la BD", e));
  }
  LeerCode() {
    this.sendwarehouseservice.LeerCode();
  }
  ngOnInit() {

  }

  async cerrarSesion() {
    await this.dbservice.destroyAllTables();
    this.trackingService.stopBackGroundService();
    this.menuCtrl.enable(false);
    this.nav.navigateRoot(['login']);
  }

  homeRedirect() {
    this.router.navigate(["main-page"]);
    this.menuCtrl.close();
  }
  profileRedirect() {
    this.router.navigate(["profile"]);
    this.menuCtrl.close();
  }
  closeMenu() {
    this.menuCtrl.close();
  }
}
