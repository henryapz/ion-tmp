import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsMapsPage } from './points-maps.page';

describe('PointsMapsPage', () => {
  let component: PointsMapsPage;
  let fixture: ComponentFixture<PointsMapsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsMapsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsMapsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
