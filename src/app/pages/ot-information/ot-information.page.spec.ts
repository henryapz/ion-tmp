import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtInformationPage } from './ot-information.page';

describe('OtInformationPage', () => {
  let component: OtInformationPage;
  let fixture: ComponentFixture<OtInformationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtInformationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtInformationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
