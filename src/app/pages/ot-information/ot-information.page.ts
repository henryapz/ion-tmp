import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PopoverController, IonSlides, NavController, AlertController, ModalController, LoadingController } from '@ionic/angular';
import { MoreOtInfoComponent } from 'src/app/components/more-ot-info/more-ot-info.component';
import { SignaturePadComponent } from 'src/app/components/signature-pad/signature-pad.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router'
import { DatabaseService } from './../../services/database.service';
import { ActivatedRoute } from '@angular/router'
import { Events } from '@ionic/angular';
import { SentAllService } from 'src/app/services/sent-all.service';
@Component({
  selector: 'app-ot-information',
  templateUrl: './ot-information.page.html',
  styleUrls: ['./ot-information.page.scss'],
})
export class OtInformationPage implements OnInit {
  @ViewChild('slider') slider: IonSlides;
  page = "0";
  estado = "3"; // test ngclass
  baseImage;
  base64FileGa;
  events;
  numberEntrega;

  sliderOptions = {
    pager: true,
    autoHeight: true,
    speed: 400,
    spaceBetween: 0
  }
  codewebGeneral: any;
  shiptoGeneral: any;
  nShipto: any;
  shiptoStatus:any;
  eventsRecojo;
  eventsEntrega;
  eventsTravel;
  reemcode: any;
  segment: string;
  options = {
    enableHighAccuracy: true
  };
  constructor(
    public popoverController: PopoverController,
    private nav: NavController,
    private alert: AlertController,
    public modalController: ModalController,
    private camera: Camera,
    private router: Router,
    private databaseService: DatabaseService,
    private geolocation: Geolocation,
    private loadingController: LoadingController,
    private eventosangular: Events,
    private syncservice: SentAllService

  ) {
    this.codewebGeneral = localStorage.getItem("codeweb");
    console.log("OT CODE on the view: ", this.codewebGeneral);
    this.shiptoGeneral = localStorage.getItem("shipto");
    console.log("SHIPTO CODE on the view: ", this.shiptoGeneral);
    this.nShipto = localStorage.getItem("nshipto");
    console.log("SHIPTO NAME on the view: ", this.nShipto);
    this.shiptoStatus = localStorage.getItem("shiptostatus");
    console.log("SHIPTO STATUS on the view: ", this.shiptoStatus);
  }

  async ngOnInit() {
    this.segment = 'recojo';
    this.numberEntrega = "1";
    this.events = [];
    this.eventsRecojo = [];
    this.eventsEntrega = [];
    this.eventsTravel = [];

    //traer eventos de la base de datos para mostrar
    await this.databaseService.getEvents().then((res: any) => {
      console.log("Eventos de la BD: " + res);
      res.forEach(element => {
        var eachElem =
        {
          "CODE": element.CODE,
          "TYPE": element.TYPE,
          "NAME": element.NAME,
          "PROCESS": element.PROCESS,
          "POSITION": element.POSITION,
          "ACTIVE": element.ACTIVE
        }
        this.events.push(eachElem)
      });

    })
    console.log("list of events", this.events)

    await this.databaseService.getLocalEventByOT(this.codewebGeneral).then((res: any) => {
      console.log("Response for Local events", res)
      res.forEach(element => {
        if (element.shipto == this.shiptoGeneral) {
          var eachElem =
          {
            "ot": element.ot,
            "shipto": element.shipto,
            "codeEvent": element.codeEvent,
            "codeType": element.codeType,
            "process": element.process,
            "latitude": element.latitude,
            "longitude": element.longitude,
            "status": element.status,
            "name": element.name,
            "datetime": element.datetime
          }
          if (element.codeType === "S") {
            this.eventsRecojo.push(eachElem)
          }

          if (element.codeType === "E") {
            this.eventsEntrega.push(eachElem)
          }

          if (element.codeType === "T") {
            this.eventsTravel.push(eachElem)
          }
        }
      })
    });
    console.log("Eventos Recojo: ", this.eventsRecojo)
    console.log("Eventos Entrega: ", this.eventsEntrega)
    console.log("Eventos Travel: ", this.eventsTravel)

    this.eventosangular.unsubscribe('closeoneventview');
    this.eventosangular.subscribe('closeoneventview', async updatedData => {
      this.shiptoStatus = "2";
      this.eventosangular.unsubscribe('closeoneventview');
    })
  }
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: MoreOtInfoComponent,
      event: ev,
      translucent: true,
      animated: true,
    });
    popover.onDidDismiss().then(data => {
      console.log(data)
    })
    return await popover.present()
  }

  selectedTab(index) {
    this.slider.slideTo(index)
    this.page = index.toString();
  }

  async moveButton() {
    let index = await this.slider.getActiveIndex();
    this.page = index.toString();
    if (index == 0) {
      this.segment = 'recojo';
    }
    if (index == 1) {
      this.segment = 'viaje';
    }
    if (index == 2) {
      this.segment = 'entrega';
    }
  }
  openRoute() {
    this.nav.navigateForward('route-maps/')
  }

  async confirmRecojo(data) {
    if(this.shiptoStatus === "1"){
      if (data.status == '1') {
        console.log("Evento recibio el parametro ", data);
        const alert = await this.alert.create({
          header: 'Confirmación!',
          message: '¿Seguro que desea proceder con el evento seleccionado?',
          buttons: [
            {
              text: 'Cancelar',
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('Confirm Cancel: blah');
              }
            }, {
              text: 'Confirmar',
              handler: async () => {
                for (let index = 0; index < this.eventsRecojo.length; index++) {
                  if (this.eventsRecojo[index].ot === data.ot && this.eventsRecojo[index].shipto === data.shipto && this.eventsRecojo[index].codeEvent === data.codeEvent) {
                    
                    this.eventosangular.unsubscribe('user:updated');
                    this.eventosangular.unsubscribe('returnedFromEdit');
                    this.eventosangular.subscribe('user:updated', async (updatedData) => {
                      console.log("respuesta",updatedData)
                      if(updatedData === 'completado' || updatedData === 'guardado'){
                        this.present()
                        await this.geolocation.getCurrentPosition(this.options).then((resp) => {
                          this.eventsRecojo[index].latitude = resp.coords.latitude;
                          this.eventsRecojo[index].longitude = resp.coords.longitude;
                          this.eventsRecojo[index].status = "2";
                          console.log("Event new", this.eventsRecojo[index])
                          
                        })
                        this.dismiss()
                      }
                      if(updatedData === 'completado'){
                        this.present()
                        await this.geolocation.getCurrentPosition(this.options).then((resp) => {
                          this.eventsRecojo[index].latitude = resp.coords.latitude;
                          this.eventsRecojo[index].longitude = resp.coords.longitude;
                          this.eventsRecojo[index].status = "2";
                          console.log("Event new", this.eventsRecojo[index])
                          
                        })
                        await this.databaseService.updateLocalEvent(this.eventsRecojo[index]).then((res) => {
                          console.log("updated event entrega", res);
                          
                        })
                        this.dismiss()
                      }
                    });
                    this.eventosangular.subscribe('returnedFromEdit', () => {
                      this.eventosangular.unsubscribe('user:updated');
                      this.eventosangular.unsubscribe('returnedFromEdit');
                  
                    });
                    localStorage.setItem("codeEvent", this.eventsRecojo[index].codeEvent)
    
                    if (this.eventsRecojo[index].process != undefined) {
                      let h = this.eventsRecojo[index].process.split(",");
                      if (h.length == 0) {
                        h.push(this.eventsRecojo[index].process);
                      }
                      console.log("Process of event selected", h);
    
                      if (h.indexOf('P') > -1) {
                        if (h.indexOf('S') > -1) {
                          localStorage.setItem("nextStep", "S")
                        }
                        const options: CameraOptions = {
                          quality: 100,
                          destinationType: this.camera.DestinationType.FILE_URI,
                          encodingType: this.camera.EncodingType.JPEG,
                          mediaType: this.camera.MediaType.PICTURE,
                          correctOrientation: true
                        }
    
                        this.camera.getPicture(options).then(async (imageData) => {
                          localStorage.setItem("photoTakenPath", imageData)
                          this.router.navigate(['view-preview']);
                        }, (err) => {
                          this.eventosangular.publish('user:updated', 'incompleto');   
                          this.eventosangular.publish('returnedFromEdit');  
                        });
                      } else {
                        if (h.indexOf('S') > -1) {
                          console.log("Sing event")
                          this.firmaRecojo();
                        }else{
                          this.eventosangular.publish('user:updated', 'completado');   
                          this.eventosangular.publish('returnedFromEdit');                             
                        }
                      }
                    } else {
                      this.eventosangular.publish('user:updated', 'completado');
                      this.eventosangular.publish('returnedFromEdit');
                    } 
                  }
                }
                
              }
            }
          ]
        });
        await alert.present();
      }
    }
  }

  async confirmEntrega(data) {
    if(this.shiptoStatus === "1"){
      if (data.status == '1') {
        console.log("Evento recibio el parametro " + data);
        const alert = await this.alert.create({
          header: 'Confirmación!',
          message: '¿Seguro que desea proceder con el evento seleccionado?',
          buttons: [
            {
              text: 'Cancelar',
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('Confirm Cancel: blah');
              }
            }, {
              text: 'Confirmar',
              handler: async () => {
                
                for (let index = 0; index < this.eventsEntrega.length; index++) {
                  if (this.eventsEntrega[index].shipto === data.shipto && this.eventsEntrega[index].codeEvent === data.codeEvent) {
                    
                    this.eventosangular.unsubscribe('user:updated');
                    this.eventosangular.unsubscribe('returnedFromEdit');
                    this.eventosangular.subscribe('user:updated', async (updatedData) => {
                      console.log("respuesta",updatedData)
                      
                      if(updatedData === 'completado' || updatedData === 'guardado'){
                        this.present()
                        await this.geolocation.getCurrentPosition(this.options).then((resp) => {
                          this.eventsEntrega[index].latitude = resp.coords.latitude;
                          this.eventsEntrega[index].longitude = resp.coords.longitude;
                          this.eventsEntrega[index].status = "2";
                          console.log("Event new", this.eventsEntrega[index])
                          
                        })
                        this.dismiss()
                      }
                      if(updatedData === 'completado'){
                        this.present()
                        await this.databaseService.updateLocalEvent(this.eventsEntrega[index]).then((res) => {
                          console.log("updated event entrega", res);
                          
                        })
                        this.dismiss()
                      }
                    });
                    this.eventosangular.subscribe('returnedFromEdit', () => {
                      this.eventosangular.unsubscribe('user:updated');
                      this.eventosangular.unsubscribe('returnedFromEdit');
                    });
                    localStorage.setItem("codeEvent", this.eventsEntrega[index].codeEvent)
                    localStorage.setItem("eventType", this.eventsEntrega[index].codeType)
                    if (this.eventsEntrega[index].process != undefined) {
                      let h = this.eventsEntrega[index].process.split(",");
                      if (h.length == 0) {
                        h.push(this.eventsEntrega[index].process);
                      }
                      console.log("Process of event selected", h);

                      if (h.indexOf('P') > -1) {
                        if (h.indexOf('S') > -1) {
                          localStorage.setItem("nextStep", "S")
                        }
                        const options: CameraOptions = {
                          quality: 100,
                          destinationType: this.camera.DestinationType.FILE_URI,
                          encodingType: this.camera.EncodingType.JPEG,
                          mediaType: this.camera.MediaType.PICTURE,
                          correctOrientation: true
                        }

                        this.camera.getPicture(options).then(async (imageData) => {
                          localStorage.setItem("photoTakenPath", imageData)
                          this.router.navigate(['view-preview']);
                        }, (err) => {
                          this.eventosangular.publish('user:updated', 'incompleto');   
                          this.eventosangular.publish('returnedFromEdit');  
                        });
                      } else {
                        if (h.indexOf('S') > -1) {
                          console.log("Sing event")
                          this.firmaRecojo();
                        }else{
                          this.eventosangular.publish('user:updated', 'completado');   
                          this.eventosangular.publish('returnedFromEdit');                             
                        }
                      }
                    } else {
                      this.eventosangular.publish('user:updated', 'completado');
                      this.eventosangular.publish('returnedFromEdit');
                    } 
                  }
                }
              }
            }
          ]
        });
        await alert.present();
      }
    }
  }

  isLoading = false;
  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: "Cargando"
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async newEvent() {
    if(this.shiptoStatus === "1"){
      console.log("Eventos: " + this.events);
      let input = { data: [] };

      for (let i = 0; i < this.events.length; i++) {
        if (this.events[i].TYPE == "T")
          input.data.push({ name: "radio" + i, type: 'radio', label: this.events[i].NAME, value: this.events[i].CODE })
      }

      const alert = await this.alert.create({
        header: 'Lista de eventos',
        inputs: input.data,
        cssClass: 'my-custom-alert',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Ok',
            handler: async (alertData) => {
              console.log('Confirm Ok: ' + alertData);
              
              for (let index = 0; index < this.events.length; index++) {
                if (this.events[index].CODE == alertData) {
                  var g = this.events[index];
                  this.eventosangular.unsubscribe('user:updated');
                  this.eventosangular.unsubscribe('returnedFromEdit');
                  this.eventosangular.subscribe('user:updated', async (updatedData) => {
                    if(updatedData === 'completado' || updatedData === 'guardado'){
                      this.present()
                      console.log("Event Selected", this.events[index])
                      var options = {
                        enableHighAccuracy: true
                      };
                      await this.geolocation.getCurrentPosition(options).then(async (resp) => {
                        var a = {
                          "ot": this.codewebGeneral,
                          "shipto": this.shiptoGeneral,
                          "codeEvent": g.CODE,
                          "codeType": g.TYPE,
                          "process": g.PROCESS,
                          "name": g.NAME,
                          "latitude": resp.coords.latitude,
                          "longitude": resp.coords.longitude,
                          "status" : "2"
                        }
                        console.log("Evento de transporte", a)
                        await this.databaseService.addLocalEvent(this.codewebGeneral, this.shiptoGeneral, a, resp.coords.latitude, resp.coords.longitude);
                        await this.eventsTravel.push(a)
                        this.dismiss()
                      })
                    }
                    
                  });
                  this.eventosangular.subscribe('returnedFromEdit', () => {
                    this.eventosangular.unsubscribe('user:updated');
                    this.eventosangular.unsubscribe('returnedFromEdit');
                  });
                  localStorage.setItem("codeEvent", this.events[index].CODE)
                  if (this.events[index].PROCESS != undefined) {
                    let h = this.events[index].PROCESS.split(",");
                    if (h.length == 0) {
                      h.push(this.events[index].PROCESS);
                    }
                    console.log("Process of event selected", h);

                    if (h.indexOf('P') > -1) {
                      if (h.indexOf('S') > -1) {
                        localStorage.setItem("nextStep", "S")
                      }
                      const options: CameraOptions = {
                        quality: 100,
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        mediaType: this.camera.MediaType.PICTURE,
                        correctOrientation: true
                      }

                      this.camera.getPicture(options).then(async (imageData) => {
                        localStorage.setItem("photoTakenPath", imageData)
                        this.router.navigate(['view-preview']);
                      }, (err) => {
                        this.eventosangular.publish('user:updated', 'incompleto');   
                        this.eventosangular.publish('returnedFromEdit');  
                      });
                    } else {
                      if (h.indexOf('S') > -1) {
                        console.log("Sing event")
                        this.firmaRecojo();
                      }else{
                        this.eventosangular.publish('user:updated', 'completado');   
                        this.eventosangular.publish('returnedFromEdit');                             
                      }
                    }
                  } else {
                    this.eventosangular.publish('user:updated', 'completado');
                    this.eventosangular.publish('returnedFromEdit');
                  } 
                  
                }
              }
            }
          }
        ]
      });

      await alert.present();
    }
  }
  async eventOther() {
    const alert = await this.alert.create({
      header: 'Especifíque',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          id: 'name2-id',
          value: 'other',
          placeholder: 'Ingrese un evento'
        },

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok', data);
            this.confirmRecojo(data.name1)
          }
        }
      ]
    });

    await alert.present();
  }


  async firmaRecojo() {
    const modal = await this.modalController.create({
      component: SignaturePadComponent,
      componentProps: { value: 123 }
    });


    return await modal.present();
  }

  takePicture() {

  }
  
  async enviarEventos() {
    this.present();
    await this.syncservice.send();
    this.dismiss();
  }

  entregaSelected(item: any) {
    console.log("Item de la remesa seleccionada", item.CODE);
    this.numberEntrega = item.OORDER + "";
    this.reemcode = item.CODE + "";
    localStorage.setItem("itemCode", item.CODE);
  }


}
