import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteMapsPage } from './route-maps.page';

describe('RouteMapsPage', () => {
  let component: RouteMapsPage;
  let fixture: ComponentFixture<RouteMapsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteMapsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteMapsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
