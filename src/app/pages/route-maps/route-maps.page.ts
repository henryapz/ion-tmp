import { Base64 } from '@ionic-native/base64/ngx';
import { DatabaseService } from 'src/app/services/database.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
declare var google;

@Component({
  selector: 'app-route-maps',
  templateUrl: './route-maps.page.html',
  styleUrls: ['./route-maps.page.scss'],
})
export class RouteMapsPage implements OnInit {

  Destination: any = '';
  MyLocation: any;
  code: String;
  shiptoCode: String;
  nshipto: String;
  constructor(
    private databaseService: DatabaseService,
    private geolocation: Geolocation
  ) {
  }

  async ngOnInit() {
    this.code = localStorage.getItem("codeweb");
    this.shiptoCode = localStorage.getItem("shipto");
    this.nshipto = localStorage.getItem("nshipto");
    let a = localStorage.getItem("latini")
    let b = localStorage.getItem("lonini")
    let c = localStorage.getItem("latfin")
    let d = localStorage.getItem("lonfin")
    var pos = {
      lat: Number(a),
      lng: Number(b)
    };
    this.MyLocation = new google.maps.LatLng(pos);
    this.Destination = new google.maps.LatLng({
      lat: Number(c),
      lng: Number(d)
    });

    this.calculateAndDisplayRoute()
  }

  async calculateAndDisplayRoute() {
    let that = this;
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    const map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: { lat: -16.394713, lng: -71.657046 }
    });
    directionsDisplay.setMap(map);
    let museumMarker;
    this.geolocation.watchPosition().subscribe((resp) => {
      if (museumMarker != undefined) {
        museumMarker.setMap(null);
      }
      const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      let filePath: string = '../assets/icon/icon-truck-black.png';
      museumMarker = new google.maps.Marker({
        position,
        title: "C",
        icon: {
          url: filePath,
          scaledSize: new google.maps.Size(30, 30)
        }
      });
      museumMarker.setMap(map);
    });
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        map.setCenter(pos);
        that.MyLocation = new google.maps.LatLng(pos);

      }, function () {

      });
    } else {
      // Browser doesn't support Geolocation
    }

    directionsService.route({
      origin: this.MyLocation,
      destination: this.Destination,
      travelMode: 'DRIVING'
    }, function (response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

}