import { File } from '@ionic-native/file/ngx';
import { DatabaseService } from './../../services/database.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  usuario: any;
  a = "gg"
  qrimage;
  constructor(
    private database: DatabaseService,
    private file: File
  ) {

  }

  async ngOnInit() {
    this.onLoad();
    await this.file.readAsDataURL(this.file.cacheDirectory, 'barcode.png').then(res => {
      this.qrimage = res;
    });
  }


  async onLoad() {
    await this.database.getDrivers().then(da => {
      console.log("Conductores de la base de datos", da);
      this.usuario = da[0];
    }, error => {
      console.log(error)
    });

    console.log("Usuario final", this.usuario);
  }
}
