import { DatabaseService } from 'src/app/services/database.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ot-detail',
  templateUrl: './ot-detail.page.html',
  styleUrls: ['./ot-detail.page.scss'],
})
export class OtDetailPage implements OnInit {
  otdetail = [];
  code: String;
  shiptoCode: String;
  nshipto: String;
  constructor(
    private databaseService: DatabaseService
  ) {

  }

  ngOnInit() {
    this.code = localStorage.getItem("codeweb");
    this.shiptoCode = localStorage.getItem("shipto");
    this.nshipto = localStorage.getItem("nshipto");
    this.databaseService.getAllOTsDetail().then((res: any) => {
      res.forEach(element => {
        if (element.SHIPTO === this.shiptoCode) {
          console.log("Detail from view: " + res)
          element.BRGEW = (element.BRGEW / 1000).toFixed(2)
          element.MATNR = Number(element.MATNR)
          this.otdetail.push(element)
        }
      })
    }).catch(console.log)

    console.log("OT details", this.otdetail)
  }

}
