import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtDetailPage } from './ot-detail.page';

describe('OtDetailPage', () => {
  let component: OtDetailPage;
  let fixture: ComponentFixture<OtDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
