import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavigationExtras, Router } from '@angular/router';
import { DatabaseService } from './../../services/database.service';
import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, Platform, Events } from '@ionic/angular';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.page.html',
  styleUrls: ['./main-page.page.scss'],
})
export class MainPagePage implements OnInit {

  ots: any = []
  buscar: any = ''
  actualCoordinates: any;
  menuisclose = true;
  a = 0;
  constructor(
    private platform: Platform,
    private nav: NavController,
    private menu: MenuController,
    private dbService: DatabaseService,
    private geolocation: Geolocation,
    private router: Router,
    private munuctrl: MenuController,
    private eventosangular: Events
  ) {
    console.log('this.router.url', this.router.url);
    this.a = 0;
    this.platform.backButton.subscribe(() => {
      this.menu.close();
      if(this.router.url == '/main-page'){
        navigator['app'].exitApp();
      }else{
        console.log("Gaaa")
      }
    })
  }

  ngOnInit() {
    this.menu.enable(true)
    this.platform.ready().then(async () => {

      this.getOTsFromDataBase();
      var options = {
        enableHighAccuracy: true
      };

      await this.geolocation.getCurrentPosition(options).then((resp) => {
        this.ots.forEach(element => {
          element.DISTANCE = "" + this.distanceInKmBetweenEarthCoordinates(resp.coords.latitude, resp.coords.longitude, element.LATSHIPTO, element.LONSHIPTO);
          element.DATEFIN = element.DATEFIN.substring(0, 16);
          element.DATEINI = element.DATEINI.substring(0, 16)
        });
      });
      console.log("Ot with distance", this.ots)
    }
    )
  }
  menustateopen() {
    this.menuisclose = false;
  }
  closemenu() {
    if(this.menu.isOpen()){
      this.menu.close();
    }
  }
  async openInfo(item: any) {
    localStorage.setItem("shipto", item.SHIPTO);
    localStorage.setItem("nshipto", item.NSHIPTO);
    localStorage.setItem("shiptostatus", item.STATUS);

    this.eventosangular.unsubscribe('waitingforclose');
    this.eventosangular.subscribe('waitingforclose', async updatedData => {
      var ggh = {
        "status":"2",
        "shipto": item.SHIPTO
      }
      
      await this.dbService.updateOTStatus(ggh).then(res => {
        console.log("Ot status updated")  
        item.STATUS = "2";
      })
      this.eventosangular.unsubscribe('waitingforclose');
    })
    this.nav.navigateForward('ot-information/')
  }
  openPoints(data: any) {
    localStorage.setItem("shipto", data.SHIPTO);
    localStorage.setItem("nshipto", data.NSHIPTO);
    localStorage.setItem("latini", data.LATCENTER)
    localStorage.setItem("lonini", data.LONCENTER)
    localStorage.setItem("latfin", data.LATSHIPTO)
    localStorage.setItem("lonfin", data.LONSHIPTO)
    this.nav.navigateForward('route-maps/')
  }
  otinformation(data: any) {
    localStorage.setItem("shipto", data.SHIPTO);
    localStorage.setItem("nshipto", data.NSHIPTO);
    this.nav.navigateForward('ot-detail/')
  }
  getOTsFromDataBase() {
    this.dbService.getAllOTs().then((res: any) => {
      console.log("OTS from service: " + res)
      res.forEach(element => {
        console.log("ot ", element)
        this.ots.push(element)
      })
    }).catch(console.log)
  }

  distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371;

    var dLat = this.degreesToRadians(lat2 - lat1);
    var dLon = this.degreesToRadians(lon2 - lon1);

    lat1 = this.degreesToRadians(lat1);
    lat2 = this.degreesToRadians(lat2);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return Math.round(earthRadiusKm * c * 10) / 10;
  }
  degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
  }
}
