import { SentAllService } from './../../services/sent-all.service';
import { PopoverController, NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-more-ot-info',
  templateUrl: './more-ot-info.component.html',
  styleUrls: ['./more-ot-info.component.scss'],
})
export class MoreOtInfoComponent implements OnInit {

  constructor(
    public popover: PopoverController,
    private nav: NavController,
    private syncservice: SentAllService
  ) {

  }

  ngOnInit() {}

  public close() {
    this.popover.dismiss()
  }
  public sync() {
    alert("Sincronizado")
    this.popover.dismiss()
  }


  public openRoute() {
    this.popover.dismiss();
    this.nav.navigateForward('route-maps/')
  }

  public enviarPhotos() {
    alert("Eventos Enviados")
    this.close()
  }

  public enviarEventos() {
    alert("Eventos enviadas")
    this.syncservice.send();
    this.close()
  }
}
