import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DatabaseService } from './../../services/database.service';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ModalController, NavController, Events } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
@Component({
  selector: 'app-signature-pad',
  templateUrl: './signature-pad.component.html',
  styleUrls: ['./signature-pad.component.scss'],
})
export class SignaturePadComponent implements OnInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 500,
    'canvasHeight': 500
  };
  constructor(
    private modalController: ModalController,
    private router: Router,
    private file: File,
    private webview: WebView,
    public databaseService: DatabaseService,
    private geolocation: Geolocation,
    private nav: NavController,
    private eventosangular: Events
  ) { }

  ngOnInit() { }
  ngAfterViewInit() {
    // this.signaturePad is now available
    this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }

  async drawComplete() {
    console.log("Data to be saved with signed", localStorage.getItem("codeweb"), localStorage.getItem("shipto"), localStorage.getItem("codeEvent"))
    var dataUrl = this.signaturePad.toDataURL();

    let name = new Date().getTime() + '.png';
    let path = this.file.cacheDirectory; //"file:///storage/emulated/0/Android/data/sider.tmp.services/cache/";
    let options: IWriteOptions = { replace: true };

    var data = dataUrl.split(',')[1];
    let blob = this.b64toBlob(data, 'image/png');

    let respuesta;
    await this.file.writeFile(path, name, blob, options).then(res => {
      console.log("rpt", res)
      respuesta = res.nativeURL;
    }, err => {
      console.log('error: ', err);
    });
    console.log("Gaaaaaa", respuesta)

    var filename = respuesta.substring(respuesta.lastIndexOf('/') + 1);
    var path2 = respuesta.substring(0, respuesta.lastIndexOf('/') + 1);
    await this.file.readAsDataURL(path2, filename).then(res => {
      console.log("Images en base64")
    });


    let photo = {
      "code": localStorage.getItem("codeweb"),
      "shipto": localStorage.getItem("shipto"),
      "eventcode": localStorage.getItem("codeEvent"),
      "path": path + name,
      "observation": ""
    }
    console.log("data for image to be saved", photo)
    await this.databaseService.addPhoto(photo).then((res) => {
      console.log("updated photo for event", res);
    })

    
    let event = {
      "ot": localStorage.getItem("codeweb"),
      "shipto": localStorage.getItem("shipto"),
      "codeEvent": localStorage.getItem("codeEvent"),
      "latitude": " ",
      "longitude": " ",
      "status": "2"
    }
    let opt = {
      enableHighAccuracy: true
    };
    await this.geolocation.getCurrentPosition(opt).then((resp) => {
      event.latitude = "" + resp.coords.latitude;
      event.longitude = "" + resp.coords.longitude;
    })
    await this.databaseService.updateLocalEvent(event).then((res) => {
      console.log("updated event", res);
    })
    this.eventosangular.publish('user:updated', 'guardado');
    let typeOfTheEvent = localStorage.getItem("eventType");
    if (typeOfTheEvent != undefined && typeOfTheEvent.includes("E")) {
      localStorage.removeItem("eventType")
      this.eventosangular.publish('waitingforclose');
      this.eventosangular.publish('closeoneventview');
    }
    
    this.modalController.dismiss()
  }
  getImagePath(imageName) {
    let path = this.file.dataDirectory + imageName;
    path = this.webview.convertFileSrc(path);
    return path;
  }
  drawStart() {
    console.log('begin drawing');
  }
  drawClear() {
    this.signaturePad.clear()
  }
  cancel() {
    this.eventosangular.publish('user:updated', 'incompleto');
    this.eventosangular.publish('returnedFromEdit');
    this.modalController.dismiss()
  }

  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
}
