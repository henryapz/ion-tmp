import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { NotLoginGuard } from './guards/not-login.guard';

const routes: Routes = [
  //{ path: '', redirectTo: 'home', pathMatch: 'full'},
  {
    path: '',
    canActivate: [AuthGuard],
    loadChildren: './login/login.module#LoginPageModule'
  },
  { path: 'login', canActivate: [NotLoginGuard], loadChildren: './login/login.module#LoginPageModule' },
  { path: 'ot-information', loadChildren: './pages/ot-information/ot-information.module#OtInformationPageModule' },
  { path: 'points-maps', loadChildren: './pages/points-maps/points-maps.module#PointsMapsPageModule' },
  { path: 'route-maps', loadChildren: './pages/route-maps/route-maps.module#RouteMapsPageModule' },
  { path: 'visualize-photo', loadChildren: './visualize-photo/visualize-photo.module#VisualizePhotoPageModule' },
  { path: 'view-preview', loadChildren: './view-preview/view-preview.module#ViewPreviewPageModule' },
  { path: 'main-page', loadChildren: './pages/main-page/main-page.module#MainPagePageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'ot-detail', loadChildren: './pages/ot-detail/ot-detail.module#OtDetailPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
