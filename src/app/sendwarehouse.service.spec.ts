import { TestBed } from '@angular/core/testing';

import { SendwarehouseService } from './sendwarehouse.service';

describe('SendwarehouseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendwarehouseService = TestBed.get(SendwarehouseService);
    expect(service).toBeTruthy();
  });
});
